/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package api

import (
	"context"
	"net/http"

	"github.com/btcsuite/btcd/btcjson"
	"gitlab.com/jaxnet/jaxnetd/network/rpc"
	"gitlab.com/jaxnet/jaxnetd/types/jaxjson"
)

var (
	rpcConfigTemplate = rpc.Config{
		ListenerAddresses: []string{},
		MaxClients:        1024,
		User:              "",
		Password:          "",
		Disable:           false,
		RPCCert:           "",
		RPCKey:            "",
		LimitPass:         "",
		LimitUser:         "",
		MaxConcurrentReqs: 1024,
		MaxWebsockets:     0,
		WSEnable:          false,
		Listeners:         nil,
	}
)

func (server *Server) runRPCServer(ctx context.Context) {
	rpcServerCtx, cancelRPCHandling := context.WithCancel(ctx)
	defer cancelRPCHandling()

	config := server.rpcConfig()
	_, err := config.SetupRPCListeners()
	if err != nil {
		panic(err)
	}

	jaxjson.RegisterOrReplaceLegacyCmd("getblocktemplate", (*btcjson.GetBlockTemplateCmd)(nil), jaxjson.UsageFlag(0))
	jaxjson.RegisterOrReplaceLegacyCmd("submitblock", (*SubmitBlockCmd)(nil), jaxjson.UsageFlag(0))

	rpcCore := rpc.NewRPCCore(config)
	rpcServeMux := http.NewServeMux()

	rpcServeMux.HandleFunc("/",
		rpcCore.HandleFunc(func(cmd *rpc.ParsedRPCCmd, closeChan <-chan struct{}) (interface{}, error) {
			method := jaxjson.LegacyMethod(cmd.Method)
			handlerFunc, ok := server.handlers()[method]
			if ok {
				return handlerFunc(rpc.CmdCtx{
					Cmd:       cmd.Cmd,
					CloseChan: closeChan,
					AuthCtx:   cmd.AuthCtx,
				})
			}

			return nil, jaxjson.ErrRPCMethodNotFound.WithMethod(method.String())
		}))

	rpcCore.StartRPC(rpcServerCtx, rpcServeMux)
}

func (server *Server) handlers() map[jaxjson.MethodName]rpc.CommandHandler {
	return map[jaxjson.MethodName]rpc.CommandHandler{
		jaxjson.LegacyMethod("getblocktemplate"): server.handleGetBlockTemplate,
		jaxjson.LegacyMethod("submitblock"):      server.handleSubmitBlock,
	}
}

func (server *Server) rpcConfig() *rpc.Config {
	var config = rpcConfigTemplate
	config.ListenerAddresses = append(config.ListenerAddresses, server.config.RPC.Interface())
	config.User = server.config.RPC.User
	config.Password = server.config.RPC.Pass
	config.DisableTLS = true
	return &config
}
