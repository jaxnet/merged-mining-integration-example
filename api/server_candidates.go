/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package api

import (
	"context"

	"gitlab.com/jaxnet/mmi/core/communicator"
	"gitlab.com/jaxnet/mmi/core/coordinator"
)

func (server *Server) handleBlocksCandidates(ctx context.Context) {
	var (
		bitcoinCandidateCachedEvent communicator.BitcoinBlockCandidate
		beaconBlockAuxCachedEvent   communicator.BeaconBlockAuxWrapper
	)

	server.propagateCoordinatorWithTheLastStateCached(
		server.coordinator, &bitcoinCandidateCachedEvent, &beaconBlockAuxCachedEvent)

	for {
		select {
		case <-ctx.Done():
			return

		case bitcoinCandidateCachedEvent = <-server.communicator.BitcoinBlockCandidates():
			server.coordinator.EnqueueBitcoinBlockCandidate(&bitcoinCandidateCachedEvent)

		case beaconBlockAuxCachedEvent = <-server.communicator.BeaconBlockAux():
			server.coordinator.EnqueueBeaconBlockHash(&beaconBlockAuxCachedEvent)
		}
	}
}

// propagateCoordinatorWithTheLastStateCached propagates current blocks templates cache
// to the newly created coordinator, so it would be able to set up its context ASAP,
// and would not wait for new blocks to arrive to fulfill the context step by step during candidates receiving.
func (server *Server) propagateCoordinatorWithTheLastStateCached(
	coord *coordinator.Coordinator,
	bitcoinCandidateEvent *communicator.BitcoinBlockCandidate,
	beaconHashEvent *communicator.BeaconBlockAuxWrapper) {

	// Candidate could be nil in case if no block templates has been reached yet.
	// In this case - no event should be propagated to the coordinator.
	if bitcoinCandidateEvent.Candidate != nil {
		coord.EnqueueBitcoinBlockCandidate(bitcoinCandidateEvent)
	}

	// Candidate could be nil in case if no block templates has been reached yet.
	// In this case - no event should be propagated to the coordinator.
	if beaconHashEvent.BeaconBlockAux != nil {
		coord.EnqueueBeaconBlockHash(beaconHashEvent)
	}
}
