/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package api

import (
	"bytes"
	"encoding/hex"

	"github.com/btcsuite/btcd/wire"
	"gitlab.com/jaxnet/jaxnetd/network/rpc"
	"gitlab.com/jaxnet/mmi/core/coordinator"
	"gitlab.com/jaxnet/mmi/core/mma"
)

func (server *Server) handleGetBlockTemplate(rpc.CmdCtx) (response interface{}, err error) {
	candidate, err := server.coordinator.BitcoinCandidate()
	if err != nil {
		return
	}

	response = candidate
	return
}

type SubmitBlockCmd struct {
	Data string `json:"data"`
}

func (server *Server) handleSubmitBlock(cmd rpc.CmdCtx) (response interface{}, err error) {
	c, ok := cmd.Cmd.(*SubmitBlockCmd)
	if !ok {
		return
	}

	server.processBlockSubmission(c.Data)
	return
}

func (server *Server) processBlockSubmission(blockHex string) (reportedErr, internalErr error) {
	blockBinary, err := hex.DecodeString(blockHex)
	if err != nil {
		reportedErr = err
		return
	}

	block := wire.MsgBlock{}
	err = block.Deserialize(bytes.NewBuffer(blockBinary))
	if err != nil {
		reportedErr = err
		return
	}

	txsHashes := coordinator.CollectBTCTxHashes(block.Transactions, true)
	client, err := mma.New(server.config)
	if err != nil {
		reportedErr = err
		return
	}

	_, err = client.SubmitCoinbase(block.Header, block.Transactions[0], txsHashes)
	if err != nil {
		reportedErr = err
		return
	}

	return
}
