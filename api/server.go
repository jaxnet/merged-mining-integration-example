/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package api

import (
	"context"

	btcdutil "github.com/btcsuite/btcutil"
	"gitlab.com/jaxnet/jaxnetd/jaxutil"
	"gitlab.com/jaxnet/jaxnetd/types/chaincfg"
	"gitlab.com/jaxnet/mmi/core/communicator"
	"gitlab.com/jaxnet/mmi/core/coordinator"
	"gitlab.com/jaxnet/mmi/core/settings"
)

type Server struct {
	context context.Context

	config       *settings.Configuration
	communicator *communicator.Communicator
	coordinator  *coordinator.Coordinator
}

func NewServer(ctx context.Context, conf *settings.Configuration) *Server {
	var btcAddress btcdutil.Address
	var jaxAddress jaxutil.Address
	var err error

	btcAddress, err = btcdutil.DecodeAddress(conf.BtcMiningAddress, conf.Btc.NetworkConfig())
	if err != nil {
		panic("unable to decode btc address")
	}

	jaxAddress, err = jaxutil.DecodeAddress(conf.JaxMiningAddress, &chaincfg.TestNet3Params)
	if err != nil {
		panic("unable to decode jax address")
	}

	return &Server{
		context:      ctx,
		config:       conf,
		communicator: communicator.New(conf),
		coordinator:  coordinator.New(btcAddress, jaxAddress, conf),
	}
}

func (server *Server) Run() {
	blockCandidatesCtx, cancelBlockCandidatesPropagation := context.WithCancel(server.context)
	go server.handleBlocksCandidates(blockCandidatesCtx)

	coordinatorCtx, cancelCoordinatorExecution := context.WithCancel(server.context)
	go server.coordinator.Run(coordinatorCtx)

	communicatorCtx, cancelCommunicatorExecution := context.WithCancel(server.context)
	go server.communicator.Run(communicatorCtx)

	rpcHandlerCtx, cancelRPCHandling := context.WithCancel(server.context)
	go server.runRPCServer(rpcHandlerCtx)

	go func() {
		if <-server.context.Done(); true {
			cancelBlockCandidatesPropagation()
			cancelCommunicatorExecution()
			cancelCoordinatorExecution()
			cancelRPCHandling()
		}
	}()
}
