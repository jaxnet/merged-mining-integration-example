module gitlab.com/jaxnet/mmi

go 1.15

require (
	github.com/btcsuite/btcd v0.22.0-beta
	github.com/btcsuite/btcutil v1.0.3-0.20201208143702-a53e38424cce
	github.com/fsnotify/fsnotify v1.5.1 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/klauspost/cpuid/v2 v2.0.6 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	gitlab.com/jaxnet/jaxnetd v0.4.5
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)
