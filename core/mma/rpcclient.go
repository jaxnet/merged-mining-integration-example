/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package mma

import (
	"bytes"
	"encoding/hex"
	"encoding/json"
	"io/ioutil"
	"math/big"
	"net/http"

	"github.com/btcsuite/btcd/wire"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
	"gitlab.com/jaxnet/jaxnetd/types/jaxjson"
	"gitlab.com/jaxnet/mmi/core/settings"
)

type ConnConfig struct {
	Host     string
	Port     uint16
	User     string
	Password string
}

type Client struct {
	// config holds the connection configuration associated with this client.
	config *ConnConfig

	// httpClient is the underlying HTTP client to use when running in HTTP
	// POST mode.
	httpClient *http.Client
}

type BeaconAuxResponse struct {
	BeaconAuxDataHex string            `json:"beacon_aux_data_hex"`
	BeaconTarget     string            `json:"beacon_target"`
	ShardsTargets    map[uint32]string `json:"shards_targets"`
}

type BeaconBlockAux struct {
	Hash            chainhash.Hash
	BeaconBigTarget big.Int
	BeaconTarget    string
	ShardBigTargets map[uint32]big.Int
	ShardTargets    map[uint32]string
}

type GetAuxResponse struct {
	Result BeaconAuxResponse
	Error  *jaxjson.RPCError `json:"error"`
	ID     string            `json:"id"`
}

type SubmitCoinbaseRequest struct {
	Jsonrpc string        `json:"jsonrpc"`
	ID      string        `json:"id"`
	Method  string        `json:"method"`
	Params  []interface{} `json:"params"`
}

type SubmitCoinbaseCmd struct {
	BlockHeaderHex string   `json:"header"`
	CoinbaseTxHex  string   `json:"coinbase_tx"`
	TXsHashesHexes []string `json:"txs_hashes"`
}

// New creates a new Merge Mining API RPC client based on the provided connection configuration details.
func New(configuration *settings.Configuration) (*Client, error) {
	httpClient := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: nil,
		},
	}

	config := &ConnConfig{
		Host:     configuration.MMA.Host,
		Port:     configuration.MMA.Port,
		User:     configuration.MMA.User,
		Password: configuration.MMA.Pass,
	}

	client := &Client{
		config:     config,
		httpClient: httpClient,
	}

	return client, nil
}

func (c *Client) GetBeaconBlockAux() (b BeaconBlockAux, err error) {
	postData := `{"jsonrpc":"2.0","id":"id","method":"getaux","params":[]}`
	postBody := bytes.NewBuffer([]byte(postData))
	req, err := http.NewRequest("POST", c.config.Host, postBody)
	if err != nil {
		return b, err
	}

	req.SetBasicAuth(c.config.User, c.config.Password)
	req.Header.Set("Content-Type", "application/json")
	response, err := c.httpClient.Do(req)
	if err != nil {
		return b, err
	}
	defer response.Body.Close()

	var body []byte
	body, err = ioutil.ReadAll(response.Body)
	if err != nil {
		return b, err
	}

	beaconAuxResponse := &GetAuxResponse{}
	err = json.Unmarshal(body, beaconAuxResponse)
	if err != nil {
		return b, err
	}

	if beaconAuxResponse.Error != nil {
		return b, err
	}

	// beaconAuxResponse.Result.BeaconAuxDataHex has markers at the start and the end of string
	// Each marker is 12 bytes long. Slice hashBytes in place to strip markers.
	hashBytes := []byte(beaconAuxResponse.Result.BeaconAuxDataHex)
	hashBytes = hashBytes[12:76]

	decoded, err := hex.DecodeString(string(hashBytes))
	if err != nil {
		return b, err
	}

	beaconBlockHash, err := chainhash.NewHash(decoded)
	if err != nil {
		return b, err
	}
	b.Hash = *beaconBlockHash

	b.BeaconTarget = beaconAuxResponse.Result.BeaconTarget
	beaconBigTarget, _ := new(big.Int).SetString(beaconAuxResponse.Result.BeaconTarget, 16)
	b.BeaconBigTarget = *beaconBigTarget

	b.ShardBigTargets = make(map[uint32]big.Int, len(beaconAuxResponse.Result.ShardsTargets))
	for k := range beaconAuxResponse.Result.ShardsTargets {
		shardTarget, _ := new(big.Int).SetString(beaconAuxResponse.Result.ShardsTargets[k], 16)
		b.ShardBigTargets[k] = *shardTarget
	}

	b.ShardTargets = make(map[uint32]string, len(beaconAuxResponse.Result.ShardsTargets))
	b.ShardTargets = beaconAuxResponse.Result.ShardsTargets

	return b, nil
}

func (c *Client) SubmitCoinbase(blockHeader wire.BlockHeader, coinbaseTx *wire.MsgTx, txsHashes []chainhash.Hash) (
	response []byte, err error) {

	buffer := bytes.NewBuffer(nil)
	err = blockHeader.Serialize(buffer)
	if err != nil {
		return
	}
	headerHex := hex.EncodeToString(buffer.Bytes())

	txsHashesHexes := make([]string, 0, len(txsHashes))
	for _, h := range txsHashes {
		txsHashesHexes = append(txsHashesHexes, h.String())
	}

	buffer = bytes.NewBuffer(nil)
	err = coinbaseTx.SerializeNoWitness(buffer)
	if err != nil {
		return
	}
	coinbaseHex := hex.EncodeToString(buffer.Bytes())

	var params = make([]interface{}, 0, len(txsHashesHexes)+2)
	params = append(params, headerHex, coinbaseHex, txsHashesHexes)

	postData := SubmitCoinbaseRequest{
		Jsonrpc: "2.0",
		ID:      "id",
		Method:  "submitbtcaux",
		Params:  params,
	}

	jsonRaw, err := json.Marshal(postData)
	if err != nil {
		return response, err
	}

	postBody := bytes.NewBuffer(jsonRaw)
	req, err := http.NewRequest("POST", c.config.Host, postBody)
	if err != nil {
		return response, err
	}

	req.SetBasicAuth(c.config.User, c.config.Password)
	req.Header.Set("Content-Type", "application/json")

	resp, err := c.httpClient.Do(req)
	if err != nil {
		return response, err
	}
	defer resp.Body.Close()

	response, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}

	return
}
