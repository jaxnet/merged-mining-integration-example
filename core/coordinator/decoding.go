/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package coordinator

import (
	"bytes"
	"encoding/hex"
	"math/big"
	"strconv"
	"time"

	"github.com/btcsuite/btcd/blockchain"
	btcdjson "github.com/btcsuite/btcd/btcjson"
	btcdchainhash "github.com/btcsuite/btcd/chaincfg/chainhash"
	btcdwire "github.com/btcsuite/btcd/wire"
	btcdutil "github.com/btcsuite/btcutil"
	"gitlab.com/jaxnet/jaxnetd/jaxutil"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
	"gitlab.com/jaxnet/mmi/core/common/utils"
	"gitlab.com/jaxnet/mmi/core/communicator"
)

var (
	fixtureEmptyNonce = uint32(0)
)

func (coordinator *Coordinator) decodeBeaconHashAux(h communicator.BeaconBlockAuxWrapper) (
	communicator.BeaconBlockAuxWrapper, error) {

	return h, nil
}

func (coordinator *Coordinator) decodeBitcoinBlockTemplate(
	c *btcdjson.GetBlockTemplateResult) (
	block *btcdwire.MsgBlock, target *big.Int, height int64, err error) {

	var beaconHash chainhash.Hash
	if coordinator.beaconBlockAuxWrapper.BeaconBlockAux == nil {
		return
	}

	// Get beaconHash from Merge Mining API method `getaux`
	beaconHash = coordinator.beaconBlockAuxWrapper.BeaconBlockAux.Hash

	height = c.Height
	block = &btcdwire.MsgBlock{}

	// Transactions processing.
	var reward, fee int64
	block.Transactions, reward, fee, err = unmarshalBitcoinTransactions(c.CoinbaseTxn, c.Transactions)
	if err != nil {
		return
	}
	if len(block.Transactions) == 0 {
		block.Transactions = make([]*btcdwire.MsgTx, 1)
	}

	if c.CoinbaseValue != nil {
		reward = *c.CoinbaseValue
	}

	var coinbaseTx *jaxutil.Tx

	// IMPORTANT: this is the most important part of the code.
	coinbaseTx, err = utils.CreateBitcoinCoinbaseTx(
		reward, fee, int32(height), coordinator.jaxRewardAddress, beaconHash[:], coordinator.config.Btc.BurnReward)
	if err != nil {
		return
	}
	cTx := utils.JaxTxToBtcTx(coinbaseTx.MsgTx())
	block.Transactions[0] = &cTx

	// Block header processing.
	previousBlockHash, err := btcdchainhash.NewHashFromStr(c.PreviousHash)
	if err != nil {
		return
	}

	bits, err := unmarshalBits(c.Bits)
	if err != nil {
		return
	}

	targetBinary, err := hex.DecodeString(c.Target)
	target = (&big.Int{}).SetBytes(targetBinary)
	if err != nil {
		return
	}

	newBlock := btcdutil.NewBlock(block)
	merkles := blockchain.BuildMerkleTreeStore(newBlock.Transactions(), false)
	block.Header = *btcdwire.NewBlockHeader(c.Version, previousBlockHash, merkles[len(merkles)-1], bits, fixtureEmptyNonce)
	block.Header.Timestamp = time.Now()
	return
}

func (coordinator *Coordinator) updateBitcoinCandidateCoinbaseTX(
	candidate *btcdjson.GetBlockTemplateResult) (err error) {

	coinbaseTx := coordinator.bitcoinCandidate.Block.Transactions[0]
	coinbaseTXBinary := bytes.NewBuffer(nil)
	coinbaseTxHash := coinbaseTx.TxHash().String()

	err = coinbaseTx.Serialize(coinbaseTXBinary)
	if err != nil {
		return
	}

	convertedCoinbaseTx := btcdutil.NewTx(coinbaseTx)
	sigOpsCost, err := blockchain.GetSigOpCost(convertedCoinbaseTx, true, nil, false, false)
	if err != nil {
		return
	}

	coordinator.bitcoinCandidate.Candidate = candidate
	coordinator.bitcoinCandidate.Candidate.CoinbaseValue = nil
	coordinator.bitcoinCandidate.Candidate.CoinbaseTxn = &btcdjson.GetBlockTemplateResultTx{
		Data:    hex.EncodeToString(coinbaseTXBinary.Bytes()),
		Hash:    coinbaseTxHash,
		Depends: []int64{},

		// The difference is when the tx is a segwit tx,
		// the calculation of hash does not include the witness data,
		// whereas the txid does.
		// (coinbase does not use segwit)
		TxID:   coinbaseTxHash,
		SigOps: int64(sigOpsCost),
		Weight: blockchain.GetTransactionWeight(convertedCoinbaseTx),
	}
	coordinator.bitcoinCandidate.Candidate.CoinbaseAux = nil

	return
}

func CollectBTCTxHashes(transactions []*btcdwire.MsgTx, witness bool) []chainhash.Hash {
	hashes := make([]chainhash.Hash, len(transactions))

	// Create the base transaction hashes and populate the array with them.
	for i, tx := range transactions {
		// If we're computing a witness merkle root, instead of the
		// regular txid, we use the modified wtxid which includes a
		// transaction's witness data within the digest. Additionally,
		// the coinbase's wtxid is all zeroes.
		switch {
		case witness && i == 0:
			var zeroHash chainhash.Hash
			hashes[i] = zeroHash
		case witness:
			wSha := tx.WitnessHash()
			copy(hashes[i][:], wSha[:])
		default:
			h := tx.TxHash()
			copy(hashes[i][:], h[:])
		}
	}

	return hashes
}

func unmarshalBitcoinTransactions(coinbaseTx *btcdjson.GetBlockTemplateResultTx,
	txs []btcdjson.GetBlockTemplateResultTx) (transactions []*btcdwire.MsgTx, reward, fee int64, err error) {

	unmarshalBitcoinTx := func(txHash string) (tx *btcdwire.MsgTx, err error) {
		txBinary, err := hex.DecodeString(txHash)
		if err != nil {
			return
		}

		tx = &btcdwire.MsgTx{}
		txReader := bytes.NewReader(txBinary)
		err = tx.Deserialize(txReader)
		return
	}

	transactions = make([]*btcdwire.MsgTx, 0, len(txs)+1)

	// Coinbase transaction must be processed first.
	// (transactions order in transactions slice is significant)
	if coinbaseTx != nil {
		cTX, err := unmarshalBitcoinTx(coinbaseTx.Data)
		if err != nil {
			return nil, 0, 0, err
		}
		for _, out := range cTX.TxOut {
			reward += out.Value
		}
		transactions = append(transactions, cTX)
	} else {
		// Coinbase transaction needs to be in any case
		transactions = append(transactions, &btcdwire.MsgTx{})
	}

	// Regular transactions processing.
	for _, marshalledTx := range txs {
		tx, err := unmarshalBitcoinTx(marshalledTx.Data)
		if err != nil {
			return nil, 0, 0, err
		}
		fee += marshalledTx.Fee
		transactions = append(transactions, tx)
	}

	return
}

func unmarshalBits(hexBits string) (bits uint32, err error) {
	var val uint64
	val, err = strconv.ParseUint(hexBits, 16, 64)
	if err != nil {
		return
	}

	bits = uint32(val)
	return
}
