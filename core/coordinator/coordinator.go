/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package coordinator

import (
	"context"
	"errors"
	"fmt"
	"math/big"
	"sync"
	"time"

	"github.com/btcsuite/btcd/blockchain"
	btcdjson "github.com/btcsuite/btcd/btcjson"
	btcdwire "github.com/btcsuite/btcd/wire"
	"github.com/btcsuite/btcutil"
	"gitlab.com/jaxnet/jaxnetd/jaxutil"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
	"gitlab.com/jaxnet/mmi/core/common/utils"
	"gitlab.com/jaxnet/mmi/core/communicator"
	"gitlab.com/jaxnet/mmi/core/settings"
)

type bitcoin struct {
	Candidate *btcdjson.GetBlockTemplateResult
	Block     *btcdwire.MsgBlock
	Target    big.Int
	Height    int64
}

type Coordinator struct {
	config                         *settings.Configuration
	incomingBitcoinBlockCandidates chan communicator.BitcoinBlockCandidate
	incomingBeaconBlockHashes      chan communicator.BeaconBlockAuxWrapper
	btcRewardAddress               btcutil.Address
	jaxRewardAddress               jaxutil.Address
	btcShouldBeBurned              bool
	bitcoinCandidate               bitcoin
	beaconBlockAuxWrapper          communicator.BeaconBlockAuxWrapper
	globalLock                     sync.RWMutex
}

func New(btcRewardAddress btcutil.Address, jaxRewardAddress jaxutil.Address, conf *settings.Configuration) (coordinator *Coordinator) {
	coordinator = &Coordinator{
		config:                         conf,
		incomingBitcoinBlockCandidates: make(chan communicator.BitcoinBlockCandidate, 16),
		incomingBeaconBlockHashes:      make(chan communicator.BeaconBlockAuxWrapper, 16),
		btcRewardAddress:               btcRewardAddress,
		jaxRewardAddress:               jaxRewardAddress,
	}

	return
}

func (coordinator *Coordinator) Run(ctx context.Context) {
	for {
		select {
		case <-ctx.Done():
			return

		case eventBitcoinCandidate := <-coordinator.incomingBitcoinBlockCandidates:
			candidateIsTheNewestOne := len(coordinator.incomingBitcoinBlockCandidates) == 0
			if !candidateIsTheNewestOne {
				continue
			}

			_ = coordinator.processBitcoinCandidate(eventBitcoinCandidate, false)

		case eventBeaconHash := <-coordinator.incomingBeaconBlockHashes:
			err := coordinator.processBeaconHashAux(eventBeaconHash)
			if err != nil {
				fmt.Println(err)
			}

		default:
			time.Sleep(time.Second * 3)
		}
	}
}

// EnqueueBitcoinBlockCandidate tries to enqueue bitcoinCandidate Block candidate for further processing.
func (coordinator *Coordinator) EnqueueBitcoinBlockCandidate(candidate *communicator.BitcoinBlockCandidate) {
	select {
	case coordinator.incomingBitcoinBlockCandidates <- *candidate:

	case <-time.After(time.Second):
		err := errors.New("can't enqueue bitcoin block candidate, queue is full")
		fmt.Println(err)
	}
}

// EnqueueBeaconBlockHash tries to enqueue beacon block candidate for further processing.
func (coordinator *Coordinator) EnqueueBeaconBlockHash(candidate *communicator.BeaconBlockAuxWrapper) {
	select {
	case coordinator.incomingBeaconBlockHashes <- *candidate:
	case <-time.After(time.Second):
		err := errors.New("can't enqueue beacon block aux, queue is full")
		fmt.Println(err)
	}
}

func (coordinator *Coordinator) BitcoinCandidate() (candidate *btcdjson.GetBlockTemplateResult, err error) {
	var candidateCopy btcdjson.GetBlockTemplateResult
	candidate = &candidateCopy

	coordinator.globalLock.RLock()
	defer coordinator.globalLock.RUnlock()

	if coordinator.bitcoinCandidate.Candidate == nil {
		err = fmt.Errorf("no bitcoin candidate is present yet")
		return
	}

	candidateCopy = *coordinator.bitcoinCandidate.Candidate
	candidateCopy.Target, err = coordinator.leastDifficultTarget()
	candidateCopy.CurTime = time.Now().Unix()

	return
}

func (coordinator *Coordinator) leastDifficultTarget() (target string, err error) {
	t, _ := (&big.Int{}).SetString("0000000000000000000000000000000000000000000000000000000000000000", 16)

	if len(coordinator.beaconBlockAuxWrapper.BeaconBlockAux.ShardTargets) > 0 {
		for k, shardTarget := range coordinator.beaconBlockAuxWrapper.BeaconBlockAux.ShardBigTargets {
			if t.Cmp(&shardTarget) < 0 {
				t = &shardTarget
				target = coordinator.beaconBlockAuxWrapper.BeaconBlockAux.ShardTargets[k]
			}
		}
	}

	if coordinator.beaconBlockAuxWrapper.BeaconBlockAux == nil {
		err = fmt.Errorf("no beacon block is present yet")
		return
	}

	if t.Cmp(&coordinator.beaconBlockAuxWrapper.BeaconBlockAux.BeaconBigTarget) < 0 {
		t = &coordinator.beaconBlockAuxWrapper.BeaconBlockAux.BeaconBigTarget
		target = coordinator.beaconBlockAuxWrapper.BeaconBlockAux.BeaconTarget
	}

	return
}

func (coordinator *Coordinator) processBitcoinCandidate(
	event communicator.BitcoinBlockCandidate, recursiveCall bool) (err error) {
	if !recursiveCall {
		coordinator.globalLock.Lock()
		defer coordinator.globalLock.Unlock()
	}

	decodedBlock, target, height, err := coordinator.decodeBitcoinBlockTemplate(event.Candidate)
	if err != nil {
		return
	}

	return coordinator.setNewBitcoinBlockCandidate(
		event.Candidate, decodedBlock, target, height)
}

func (coordinator *Coordinator) setNewBitcoinBlockCandidate(
	candidate *btcdjson.GetBlockTemplateResult,
	decodedBlock *btcdwire.MsgBlock, target *big.Int, height int64) (err error) {

	defer func() {
		revertBlockCandidateState := func() {
			coordinator.bitcoinCandidate = bitcoin{}
		}

		if coordinator.bitcoinCandidate.Block == nil ||
			coordinator.bitcoinCandidate.Candidate == nil {
			revertBlockCandidateState()
		}
	}()

	var (
		beaconHash   chainhash.Hash
		beaconTarget big.Int
	)

	if coordinator.beaconBlockAuxWrapper.BeaconBlockAux == nil {
		return
	}
	beaconHash = coordinator.beaconBlockAuxWrapper.BeaconBlockAux.Hash
	beaconTarget = coordinator.beaconBlockAuxWrapper.BeaconBlockAux.BeaconBigTarget

	coordinator.bitcoinCandidate.Target = beaconTarget
	coordinator.bitcoinCandidate.Height = height

	coordinator.bitcoinCandidate.Block = decodedBlock
	err = utils.IncludeBeaconHashIntoBitcoinCoinbaseTX(
		coordinator.bitcoinCandidate.Block,
		coordinator.bitcoinCandidate.Height,
		0x00,
		beaconHash[:])

	if err != nil {
		return
	}

	err = coordinator.updateBitcoinCandidateCoinbaseTX(candidate)
	return
}

func (coordinator *Coordinator) setNewBeaconHashAux(b communicator.BeaconBlockAuxWrapper) (err error) {
	defer func() {
		revertBlockCandidateState := func() {
			coordinator.beaconBlockAuxWrapper = communicator.BeaconBlockAuxWrapper{}
		}

		if b.BeaconBlockAux == nil {
			revertBlockCandidateState()
		} else {
			coordinator.beaconBlockAuxWrapper = b
		}
	}()

	return
}

func (coordinator *Coordinator) setBTCExtraNonce(
	block *btcdwire.MsgBlock, extraNonce []byte) {
	signatureScript := block.Transactions[0].TxIn[0].SignatureScript

	const heightLenIdx = 0
	heightLen := int(signatureScript[heightLenIdx])
	if heightLen > 0xF {
		heightLen = 0
	}
	extraNonceLenIdx := heightLenIdx + 1 + heightLen
	extraNonceLen := int(signatureScript[extraNonceLenIdx])

	head := signatureScript[0:extraNonceLenIdx]
	tail := signatureScript[extraNonceLenIdx+extraNonceLen+1:]

	coinbaseScript := append(head, byte(len(extraNonce)))
	coinbaseScript = append(coinbaseScript, extraNonce...)
	coinbaseScript = append(coinbaseScript, tail...)

	block.Transactions[0].TxIn[0].SignatureScript = coinbaseScript

	// Recalculate the merkle root with the updated extra nonce.
	newBlock := btcutil.NewBlock(block)
	merkleTree := blockchain.BuildMerkleTreeStore(newBlock.Transactions(), false)
	block.Header.MerkleRoot = *merkleTree[len(merkleTree)-1]
}

func (coordinator *Coordinator) processBeaconHashAux(event communicator.BeaconBlockAuxWrapper) (err error) {
	coordinator.globalLock.Lock()
	defer coordinator.globalLock.Unlock()

	hashAux, err := coordinator.decodeBeaconHashAux(event)
	if err != nil {
		return
	}

	err = coordinator.setNewBeaconHashAux(hashAux)
	if err != nil {
		return
	}

	var (
		btcCandidateEvent   communicator.BitcoinBlockCandidate
		btcCandidatePresent bool
	)

	if coordinator.bitcoinCandidate.Candidate != nil {
		btcCandidateEvent = communicator.BitcoinBlockCandidate{Candidate: coordinator.bitcoinCandidate.Candidate}
		btcCandidatePresent = true
	}

	if btcCandidatePresent {
		err = coordinator.processBitcoinCandidate(btcCandidateEvent, true)
		if err != nil {
			return
		}
	}

	return
}
