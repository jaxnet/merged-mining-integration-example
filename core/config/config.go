/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package config

import (
	"flag"
	"io/ioutil"

	"gitlab.com/jaxnet/mmi/core/settings"
	"gopkg.in/yaml.v3"
)

func LoadConfig() (config *settings.Configuration, err error) {
	path := flag.String("config", "conf.yaml", "path to configuration file")
	flag.Parse()

	data, err := ioutil.ReadFile(*path)
	if err != nil {
		return nil, err
	}

	config = &settings.Configuration{}
	err = yaml.Unmarshal(data, config)
	if err != nil {
		return
	}

	return
}
