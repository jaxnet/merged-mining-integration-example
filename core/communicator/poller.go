/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package communicator

import (
	"context"
	"time"

	btcdjson "github.com/btcsuite/btcd/btcjson"
	"gitlab.com/jaxnet/jaxnetd/types/jaxjson"
	"gitlab.com/jaxnet/mmi/core/settings"
)

const (
	AvgBeaconBlockMiningIntervalSeconds  = 60 * 10 // 10 minutes
	AvgBitcoinBlockMiningIntervalSeconds = 60 * 10 // 10 minutes

	pollingIntervalSeconds    = 4
	beaconAuxEventsSlotsCount = AvgBeaconBlockMiningIntervalSeconds / pollingIntervalSeconds
	bitcoinEventsSlotsCount   = AvgBitcoinBlockMiningIntervalSeconds / pollingIntervalSeconds
)

type poller struct {
	config                       *settings.Configuration
	jaxOptions                   *jaxjson.TemplateRequest
	btcOptions                   *btcdjson.TemplateRequest
	beaconBlockAux               chan BeaconBlockAuxWrapper
	bitcoinBlockCandidates       chan BitcoinBlockCandidate
	lastBitcoinPreviousBlockHash string
	lastBeaconPreviousBlockHash  string
}

func newPoller(conf *settings.Configuration) (p *poller) {
	// capability:
	//  case "coinbasetxn":      hasCoinbaseTxn = true
	//  case "coinbasevalue":    hasCoinbaseValue = true
	//  case "burnbtcreward":    burnReward |= types.BurnBtcReward
	//  case "burnjaxnetreward": burnReward |= types.BurnJaxNetReward
	//  case "burnjaxreward":    burnReward |= types.BurnJaxReward
	jaxRequestOptions := &jaxjson.TemplateRequest{
		Mode: "template",
		Capabilities: []string{
			"coinbasetxn",
		},
	}

	if conf.Btc.BurnReward {
		jaxRequestOptions.Capabilities = append(jaxRequestOptions.Capabilities, "burnbtcreward", "burnjaxnetreward")
	} else {
		jaxRequestOptions.Capabilities = append(jaxRequestOptions.Capabilities, "burnjaxreward")
	}

	btcRequestOptions := &btcdjson.TemplateRequest{
		Mode: "template",
		Capabilities: []string{
			"coinbasevalue",
		},
		WorkID: "coinbase/append",
		Rules:  []string{"segwit"},
	}

	return &poller{
		config: conf,

		jaxOptions: jaxRequestOptions,
		btcOptions: btcRequestOptions,

		beaconBlockAux:         make(chan BeaconBlockAuxWrapper, beaconAuxEventsSlotsCount),
		bitcoinBlockCandidates: make(chan BitcoinBlockCandidate, bitcoinEventsSlotsCount),
	}
}

func (poller *poller) Run(ctx context.Context) {
	btcPollingCtx, cancelBTCBlocksPolling := context.WithCancel(ctx)
	go poller.handleBitcoinBlockCandidatesPolling(btcPollingCtx)

	beaconPollingCtx, cancelBeaconBlocksPolling := context.WithCancel(ctx)
	go poller.handleBeaconBlockAuxPolling(beaconPollingCtx)

	stop := func() {
		cancelBTCBlocksPolling()
		cancelBeaconBlocksPolling()
		// ...
		// Stop all other internal processes here.
	}

	if <-ctx.Done(); true {
		stop()
	}
}

func (poller *poller) handleBitcoinBlockCandidatesPolling(ctx context.Context) {
	var (
		err           error
		candidate     *btcdjson.GetBlockTemplateResult
		btcNodeClient = connectToBTCNode(poller.config)

		intervalBasedCandidateResendingCtx    context.Context
		cancelIntervalBasedCandidateResending context.CancelFunc
	)

	// beginCandidateSendingEveryTimeInterval sends fetched block candidate for further processing
	// and starts re-sending the same candidate every 10 seconds.
	// Re-sending is needed to force coordinator to reset candidate timestamp.
	// Resetting timestamp is required to prevent errors when no more than N shard blocks could be closed
	// with the same AUX block (BTC block candidate that was generated some time ago).
	beginCandidateSendingEveryTimeInterval := func(ctx context.Context) {

		// Send fetched candidate immediately.
		poller.enqueueBitcoinBlock(candidate)

		// Begin resending it with time interval
		// until new candidate would be fetched and the ctx would be cancelled.
		for {
			select {
			case <-ctx.Done():
				return

			case <-time.After(time.Second * 10):
				poller.enqueueBitcoinBlock(candidate)
			}
		}
	}

	for {
		select {
		case <-ctx.Done():
			// If no even one BTC candidate was received — no cancel function is present yet.
			// The edge case here is process ending: this method should not be called if no BTC candidate is there.
			if cancelIntervalBasedCandidateResending != nil {
				cancelIntervalBasedCandidateResending()
			}

			return

		case <-time.Tick(time.Second * pollingIntervalSeconds):
			candidate, err = btcNodeClient.GetBlockTemplate(poller.btcOptions)
			if err != nil {

				// Attempt to reconnect to the node.
				btcNodeClient = connectToBTCNode(poller.config)
				candidate, err = btcNodeClient.GetBlockTemplate(poller.btcOptions)
				if err != nil {
					continue
				}
			}

			passes := poller.passBitcoinBlockThroughDuplicatesFilter(candidate)
			if passes {
				// New bitcoin candidate fetched.
				// Stop previous timed interval updates handler
				if cancelIntervalBasedCandidateResending != nil {
					cancelIntervalBasedCandidateResending()
				}

				// Start new interval based handler
				intervalBasedCandidateResendingCtx, cancelIntervalBasedCandidateResending = context.WithCancel(ctx)
				go beginCandidateSendingEveryTimeInterval(intervalBasedCandidateResendingCtx)
			}
		}
	}
}

func (poller *poller) handleBeaconBlockAuxPolling(ctx context.Context) {
	var (
		err error
		// beaconBlockAux BeaconBlockAuxWrapper
		// beaconBlockAux common.BeaconBlockAux

		mmaClient = connectToMMApi(poller.config)
	)

	// First iteration should be executed without interval timeout.
	// In case of any error — reconnection attempt would be done on next iterations.
	beaconBlockAux, err := mmaClient.GetBeaconBlockAux()
	if err == nil {
		poller.enqueueBeaconBlockAux(BeaconBlockAux(beaconBlockAux))
	}

	for {
		select {
		case <-ctx.Done():
			return

		case <-time.Tick(time.Second * pollingIntervalSeconds):
			beaconBlockAux, err = mmaClient.GetBeaconBlockAux()
			if err != nil {
				// Attempt to reconnect to the node.
				mmaClient = connectToMMApi(poller.config)
				beaconBlockAux, err = mmaClient.GetBeaconBlockAux()
				if err != nil {
					continue
				}
			}

			poller.enqueueBeaconBlockAux(BeaconBlockAux(beaconBlockAux))
		}
	}
}

func (poller *poller) passBitcoinBlockThroughDuplicatesFilter(candidate *btcdjson.GetBlockTemplateResult) (passes bool) {
	passes = poller.lastBitcoinPreviousBlockHash != candidate.PreviousHash
	if passes {
		// Prevent new duplicates.
		poller.lastBitcoinPreviousBlockHash = candidate.PreviousHash
	}

	return
}

func (poller *poller) passBeaconBlockThroughDuplicatesFilter(candidate *jaxjson.GetBlockTemplateResult) (passes bool) {
	passes = poller.lastBeaconPreviousBlockHash != candidate.PreviousHash
	if passes {
		// Prevent new duplicates.
		poller.lastBeaconPreviousBlockHash = candidate.PreviousHash
	}

	return
}

func (poller *poller) enqueueBitcoinBlock(candidate *btcdjson.GetBlockTemplateResult) {
	// Block candidates could be processed slower than they arrive.
	// in this case, candidates queue would be overflown.
	// It is better to skip this block totally and even not to enqueue it for processing,
	// cause with high probability when it would reach the processing —
	// new candidate would be available and this one would be an obsolete one.
	select {
	case poller.bitcoinBlockCandidates <- BitcoinBlockCandidate{Candidate: candidate}:
	}
}

func (poller *poller) enqueueBeaconBlockAux(beaconBlockAux BeaconBlockAux) {
	// Block candidates could be processed slower than they arrive.
	// in this case, candidates queue would be overflown.
	// It is better to skip this block totally and even not to enqueue it for processing,
	// cause with high probability when it would reach the processing —
	// new candidate would be available and this one would be an obsolete one.
	select {
	case poller.beaconBlockAux <- BeaconBlockAuxWrapper{BeaconBlockAux: &beaconBlockAux}:
	}
}
