/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package communicator

import (
	"fmt"

	btcdrpcc "github.com/btcsuite/btcd/rpcclient"
	"gitlab.com/jaxnet/mmi/core/mma"
	"gitlab.com/jaxnet/mmi/core/settings"
)

// connectToBTCNode attempts to initialize RPC connection with the BTC node.
// Returns connection instance in case of success.
// In case of error prints error to STDOUT.
func connectToBTCNode(conf *settings.Configuration) (client *btcdrpcc.Client) {
	var err error
	client, err = btcdrpcc.New(conf.Btc.RPCConf(), nil)
	if err != nil {
		fmt.Println(err)
		return nil
	}

	return
}

// connectToMMApi attempts to initialize RPC connection with the Merge Mining API.
// Returns connection instance in case of success.
// In case of error prints error to STDOUT.
func connectToMMApi(conf *settings.Configuration) (client *mma.Client) {
	var err error
	client, err = mma.New(conf)
	if err != nil {
		fmt.Println(err)
		return nil
	}

	return
}
