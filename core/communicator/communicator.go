/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package communicator

import (
	"context"

	"gitlab.com/jaxnet/mmi/core/settings"
)

// Communicator implements main communication layer with BTC node and Merge Mining API.
type Communicator struct {
	poller *poller
}

func New(conf *settings.Configuration) (communicator *Communicator) {
	communicator = &Communicator{
		poller: newPoller(conf),
	}

	return
}

func (c *Communicator) Run(ctx context.Context) {
	pollerCtx, cancelPollerExecution := context.WithCancel(ctx)
	go c.poller.Run(pollerCtx)

	stop := func() {
		cancelPollerExecution()
	}

	select {
	case <-ctx.Done():
		stop()

	case <-pollerCtx.Done():
		stop()
	}
}

func (c *Communicator) BitcoinBlockCandidates() <-chan BitcoinBlockCandidate {
	return c.poller.bitcoinBlockCandidates
}

func (c *Communicator) BeaconBlockAux() <-chan BeaconBlockAuxWrapper {
	return c.poller.beaconBlockAux
}
