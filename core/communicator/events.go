/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package communicator

import (
	"math/big"

	btcdjson "github.com/btcsuite/btcd/btcjson"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
)

type BeaconBlockAux struct {
	Hash            chainhash.Hash
	BeaconBigTarget big.Int
	BeaconTarget    string
	ShardBigTargets map[uint32]big.Int
	ShardTargets    map[uint32]string
}

type BeaconBlockAuxWrapper struct {
	BeaconBlockAux *BeaconBlockAux
}

type BitcoinBlockCandidate struct {
	Candidate *btcdjson.GetBlockTemplateResult
}
