/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package settings

import (
	"fmt"
)

type CredentialsConfiguration struct {
	User string `yaml:"user"`
	Pass string `yaml:"pass"`
}

type NetworkInterfaceConfiguration struct {
	Host string `yaml:"host"`
	Port uint16 `yaml:"port"`
}

func (c *NetworkInterfaceConfiguration) Interface() string {
	return fmt.Sprint(c.Host, ":", c.Port)
}
