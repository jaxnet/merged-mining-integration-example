/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package settings

type Configuration struct {
	BtcMiningAddress string               `yaml:"btc_mining_address"`
	JaxMiningAddress string               `yaml:"jax_mining_address"`
	MMA              MMAConfiguration     `yaml:"mma"`
	RPC              RPCConfiguration     `yaml:"rpc"`
	Btc              BitcoinConfiguration `yaml:"btc"`
}
