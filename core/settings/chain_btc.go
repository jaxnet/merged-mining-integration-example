/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package settings

import (
	btcdchaincfg "github.com/btcsuite/btcd/chaincfg"
	btcdrpcclient "github.com/btcsuite/btcd/rpcclient"
)

type BitcoinConfiguration struct {
	CredentialsConfiguration      `yaml:",inline"`
	NetworkInterfaceConfiguration `yaml:",inline"`

	Network    string `yaml:"network"`
	BurnReward bool   `yaml:"burn-reward"`
}

// RPCConf returns RPC connection parameters of the beacon chain
// (in a format that is appropriate for the RPC-client).
func (c *BitcoinConfiguration) RPCConf() (conf *btcdrpcclient.ConnConfig) {
	conf = c.defaultRPCConfig()
	conf.Host = c.Interface()
	conf.User = c.User
	conf.Pass = c.Pass
	return
}

// defaultRPCConfig returns RPC connection parameters
// that are common for beacon chain and all shard chains.
func (c *BitcoinConfiguration) defaultRPCConfig() *btcdrpcclient.ConnConfig {
	return &btcdrpcclient.ConnConfig{
		Params:       c.Network,
		HTTPPostMode: true,
		DisableTLS:   true,
	}
}

// NetworkConfig returns configuration according to the network name specified in the config,
// or panics if unexpected network specified.
func (c *BitcoinConfiguration) NetworkConfig() (cfg *btcdchaincfg.Params) {
	switch {
	case c.Network == "mainnet":
		return &btcdchaincfg.MainNetParams
	case c.Network == "testnet3":
		return &btcdchaincfg.TestNet3Params
	case c.Network == "simnet":
		return &btcdchaincfg.SimNetParams
	}

	panic("unexpected BTC network occurred, expected mainnet")
}
