/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package settings

type RPCConfiguration struct {
	CredentialsConfiguration      `yaml:",inline"`
	NetworkInterfaceConfiguration `yaml:",inline"`
}

type MMAConfiguration struct {
	CredentialsConfiguration      `yaml:",inline"`
	NetworkInterfaceConfiguration `yaml:",inline"`
}
