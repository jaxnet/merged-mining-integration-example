/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package settings

type MiningConfiguration struct {
	RewardAddress string `yaml:"reward-address"`
}
