package utils

import (
	"encoding/binary"
	"errors"
	"fmt"

	"github.com/btcsuite/btcd/blockchain"
	btcdwire "github.com/btcsuite/btcd/wire"
	btcdutil "github.com/btcsuite/btcutil"
	"gitlab.com/jaxnet/jaxnetd/jaxutil"
	"gitlab.com/jaxnet/jaxnetd/node/chaindata"
	"gitlab.com/jaxnet/jaxnetd/txscript"
	"gitlab.com/jaxnet/jaxnetd/types"
	"gitlab.com/jaxnet/jaxnetd/types/chaincfg"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
	"gitlab.com/jaxnet/jaxnetd/types/wire"
)

// CoinbaseFlags is added to the coinbase script of a generated block
// and is used to monitor BIP16 support as well as blocks that are
// generated via Jax node.
const CoinbaseFlags = "/P2SH/jaxnetd/"

var JaxnetScriptSigMarkerBytes = []byte{0x6a, 0x61, 0x78, 0x6e, 0x65, 0x74}

func IncludeBeaconHashIntoBitcoinCoinbaseTX(block *btcdwire.MsgBlock, height int64, extraNonce uint64, beaconHash []byte) (err error) {
	coinbaseScript, err := chaindata.BTCCoinbaseScript(height, PackUint64LE(extraNonce), beaconHash)
	if err != nil {
		return err
	}
	if len(coinbaseScript) > chaindata.MaxCoinbaseScriptLen {
		return fmt.Errorf("coinbase transaction script length of %d is out of range (min: %d, max: %d)",
			len(coinbaseScript), chaindata.MinCoinbaseScriptLen, chaindata.MaxCoinbaseScriptLen)
	}

	block.Transactions[0].TxIn[0].SignatureScript = coinbaseScript

	// Recalculate the merkle root with the updated extra nonce.
	newBlock := btcdutil.NewBlock(block)
	merkles := blockchain.BuildMerkleTreeStore(newBlock.Transactions(), false)
	block.Header.MerkleRoot = *merkles[len(merkles)-1]

	return
}

// CreateBitcoinCoinbaseTx is the most important part of the code.
func CreateBitcoinCoinbaseTx(value, fee int64, nextHeight int32, addr jaxutil.Address, beaconHash []byte, burnReward bool) (*jaxutil.Tx, error) {
	btcCoinbase, err := CreateJaxCoinbaseTx(value, fee, nextHeight, 0, addr, burnReward, false)
	if err != nil {
		return nil, err
	}
	btcCoinbase.MsgTx().TxIn[0].SignatureScript, err = BTCCoinbaseScript(int64(nextHeight), make([]byte, 8), beaconHash)
	return btcCoinbase, err
}

func CreateJaxCoinbaseTx(value, fee int64, height int32,
	shardID uint32, addr jaxutil.Address, burnReward, beacon bool) (*jaxutil.Tx, error) {
	return CreateJaxCoinbaseTxWithBurn(value, fee, height, shardID, addr, burnReward, beacon, types.RawJaxBurnScript)
}

func CreateJaxCoinbaseTxWithBurn(value, fee int64, height int32,
	shardID uint32, addr jaxutil.Address, burnReward, beacon bool, jaxBurnScript []byte) (*jaxutil.Tx, error) {
	extraNonce := uint64(0)
	coinbaseScript, err := StandardCoinbaseScript(height, shardID, extraNonce)
	if err != nil {
		return nil, err
	}

	feeAddress, err := txscript.PayToAddrScript(addr)
	if err != nil {
		return nil, err
	}

	pkScript := feeAddress
	if burnReward {
		pkScript = jaxBurnScript
	}

	tx := wire.NewMsgTx(wire.TxVersion)
	tx.AddTxIn(&wire.TxIn{
		// Coinbase transactions have no inputs, so previous outpoint is
		// zero hash and max index.
		PreviousOutPoint: *wire.NewOutPoint(&chainhash.Hash{}, wire.MaxPrevOutIndex),
		SignatureScript:  coinbaseScript,
		Sequence:         wire.MaxTxInSequenceNum,
	})

	tx.AddTxOut(&wire.TxOut{Value: 0, PkScript: jaxBurnScript})
	if beacon {
		switch addr.(type) {
		case *jaxutil.AddressPubKey:
		case *jaxutil.AddressPubKeyHash:
		case *jaxutil.AddressScriptHash:
		default:
			return nil, errors.New("miner address must be *AddressPubKeyHash, *AddressPubKey or *AddressScriptHash")
		}

		const baseReward = chaincfg.BeaconBaseReward * int64(chaincfg.HaberStornettaPerJAXNETCoin)
		lockScript, err := txscript.HTLCScript(addr, chaincfg.BeaconRewardLockPeriod)
		if err != nil {
			return nil, err
		}

		tx.AddTxOut(&wire.TxOut{Value: baseReward, PkScript: pkScript})
		tx.AddTxOut(&wire.TxOut{Value: value - baseReward, PkScript: lockScript})
	} else {
		tx.AddTxOut(&wire.TxOut{Value: value, PkScript: pkScript})
	}

	tx.AddTxOut(&wire.TxOut{Value: fee, PkScript: feeAddress})

	return jaxutil.NewTx(tx), nil
}

func StandardCoinbaseScript(nextBlockHeight int32, shardID uint32, extraNonce uint64) ([]byte, error) {
	return txscript.NewScriptBuilder().
		AddInt64(int64(nextBlockHeight)).
		AddInt64(int64(shardID)).
		AddInt64(int64(extraNonce)).
		AddData([]byte(CoinbaseFlags)).
		Script()
}

// BTCCoinbaseScript returns a standard script suitable for use as the
// signature script of the coinbase transaction of a new block.  In particular,
// it starts with the block height that is required by version 2 blocks and adds
// the extra nonce as well as additional coinbase flags.
func BTCCoinbaseScript(nextBlockHeight int64, extraNonce, beaconHash []byte) ([]byte, error) {
	return txscript.NewScriptBuilder().
		AddInt64(nextBlockHeight).
		AddData(extraNonce).
		AddData(JaxnetScriptSigMarkerBytes).
		AddData(beaconHash).
		AddData(JaxnetScriptSigMarkerBytes).
		AddData([]byte(CoinbaseFlags)).
		Script()
}

func PackUint64LE(n uint64) []byte {
	b := make([]byte, 8)
	binary.LittleEndian.PutUint64(b, n)
	return b
}
