package utils

import (
	btcdchainhash "github.com/btcsuite/btcd/chaincfg/chainhash"
	btcdwire "github.com/btcsuite/btcd/wire"
	"gitlab.com/jaxnet/jaxnetd/types/wire"
)

func JaxTxToBtcTx(tx *wire.MsgTx) btcdwire.MsgTx {
	msgTx := btcdwire.MsgTx{
		Version:  tx.Version,
		TxIn:     make([]*btcdwire.TxIn, len(tx.TxIn)),
		TxOut:    make([]*btcdwire.TxOut, len(tx.TxOut)),
		LockTime: tx.LockTime,
	}

	for i := range msgTx.TxIn {
		msgTx.TxIn[i] = &btcdwire.TxIn{
			PreviousOutPoint: btcdwire.OutPoint{
				Hash:  btcdchainhash.Hash(tx.TxIn[i].PreviousOutPoint.Hash),
				Index: tx.TxIn[i].PreviousOutPoint.Index,
			},
			SignatureScript: tx.TxIn[i].SignatureScript,
			Witness:         btcdwire.TxWitness(tx.TxIn[i].Witness),
			Sequence:        tx.TxIn[i].Sequence,
		}
	}

	for i := range msgTx.TxOut {
		msgTx.TxOut[i] = &btcdwire.TxOut{
			Value:    tx.TxOut[i].Value,
			PkScript: tx.TxOut[i].PkScript,
		}
	}
	return msgTx
}
