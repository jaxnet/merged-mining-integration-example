package main

import (
	"context"
	"os"

	"gitlab.com/jaxnet/mmi/api"
	"gitlab.com/jaxnet/mmi/core/config"
)

func main() {
	configuration, _ := config.LoadConfig()
	ctx, cancel := context.WithCancel(context.Background())
	server := api.NewServer(ctx, configuration)
	server.Run()

	for {
		select {
		case <-ctx.Done():
			cancel()
			os.Exit(0)
		}
	}
}
